/* 
 * Copyright (C) 2014 Xer0z
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : CifradoAES
 * Created on : 26-jul-2014, 13:36:00
 * Author     : Raúl Sempere Trujillo
 */

package admintool;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Hex;

/**
 *Clase utilziada para cifrar y descifrar usando AES
 * 
 * @author Raúl Sempere Trujillo
 */
public class CifradoAES
{

    /**
     * Al publicar el código fuente, poner la clave secreta aquí es absurdo, 
     * hasta que esto se arregle, es necesario modificarla y compilar tu propio
     * AdminTool si no se quiere que la clave utilizada sea pública.
     * 
     * Clave de cifrado usando el máximo tamaño para AES 128bits -> 8*16 = 128
     */
    private static final SecretKeySpec clave = new SecretKeySpec("RST-tfgEPSUA2014".getBytes(), "AES");
    
    /**
     * Cifra con AES el texto plano pasado por parámentro.
     * 
     * @param textoPlano String con el texto a cifrar
     * @return String con el texto cifrado resultante (Hexadecimal)
     */
    public static String cifrar(String textoPlano)
    {
        String textoCifrado = "";
        try
        {
            Cipher cipher = Cipher.getInstance("AES");

            cipher.init(Cipher.ENCRYPT_MODE, clave);
            byte[] bytesCifrados = cipher.doFinal(textoPlano.getBytes());
            
            textoCifrado = Hex.encodeHexString(bytesCifrados);
        }
        catch (Exception ex)
        {
           ex.printStackTrace();
        }
        
        return textoCifrado;
    }
    
    /**
     * Descifra con AES el texto cifrado pasado por parámentro
     * 
     * @param textoCifrado String con el texto a descifrar
     * @return String con el texto plano despues de descifrar
     */
    public static String descifrar(String textoCifrado)
    {
        String textoPlano = "";
        try
        {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, clave);
            
            byte[] datosDecifrados = cipher.doFinal(Hex.decodeHex(textoCifrado.toCharArray()));
            textoPlano = new String(datosDecifrados);
        }
        catch (Exception ex)
        {
           ex.printStackTrace();
        }
        
        return textoPlano;
    }
}
