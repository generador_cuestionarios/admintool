/* 
 * Copyright (C) 2014 Xer0z
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Document   : Main
 * Created on : 26-jul-2014, 13:36:00
 * Author     : Raúl Sempere Trujillo
 */

package admintool;

import CEN.*;
import EN.*;
import clases_generador.AccesoBD;
import clases_generador.CADException;
import clases_generador.IO;
import java.util.ArrayList;
import javax.swing.JPasswordField;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Clase principal de la aplicacion AdminTool, contiene todas las funcionalidades
 * de la herramienta.
 * 
 * @author Raúl Sempere Trujillo
 */
public class Main extends Thread
{
    /**
     * Variable privada: campo de texto donde escribe el usuario en el formulario
     */
    private JPasswordField txtComando = null;
    /**
     * Variable privada: Consola que se muestra al usuario
     */
    private JTextArea txtConsola = null;
    /**
     * Variable privada: Scroll de la consola, necesario para que la clase Main
     * pueda reubicar su posición
     */
    private JScrollPane scrollPrincipal = null;
    /**
     * Variable privada: Indica a la clase main si se ha escrito un nuevo comando
     * y por lo tanto debe ejecutarlo
     */
    private boolean nuevoComando =  false;
    /**
     * Variable privada: Ruta del fichero de configuración
     */
    private final String rutaCfg = "./cipheredConfig.cfg";
    /**
     * Variable privada: UsuarioEN Logueado en la herramienta
     */
    private UsuarioEN usuarioLogin = null;
    
    /**
     * Función inicial de la herramienta, llama a la función iniciar y cuando esta termina
     * (el usuario acaba) se sale de este hilo de la clase Main
     */
    @Override public void run()
    {
        try
        {  
            iniciar();
            System.exit(0);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    /**
     * Lanza todo el proceso de la aplicación
     * 
     * @throws CAD.CADException Error al acceder a la base de datos
     */
    private void iniciar() throws CADException
    {
        escribirnl("Bienvenido al sistema de administración de la "
            + "herramienta de generación de cuestionarios.\n");
        
        int opcionInicioSesion = 0;
        
        //Mostramos el menú de inicio de sesión tantas veces como el usuario quiera
        while (usuarioLogin == null && opcionInicioSesion != 3)
        {
            opcionInicioSesion = menuIniciarSesion();
            
            switch (opcionInicioSesion)
            {
                case 1: 
                    usuarioLogin = iniciarSesion();
                    break;
                case 2:
                    nuevaCadenaConexion();
                    break;
            }
        }
        
        //Si hemos terminado de iniciar sesión comprobamos si ha habido éxito
        if (usuarioLogin != null)
        {
            int opcion = menuPrincipal();
            
            //Mostramos el menú principal hasta que se decida salir
            while (opcion != 4)
            {
                switch(opcion)
                {
                    //Asignaturas
                    case 1:
                        gestionAsignaturas();
                        break;
                    //Usuarios
                    case 2:
                        gestionUsuarios();
                        break;
                    //Idiomas
                    case 3:
                        gestionIdiomas();
                        break;
                }
                
                opcion = menuPrincipal(); 
            }
        }
    }
    
    /**
     * Rellena un texto con espacios por la derecha, utilizado para simular
     * tablas en la consola y que se muestre el texto correctamente
     * 
     * @param texto String con el texto que se rellenará con espacios
     * @param tam int tamaño total del texto incluidos los espacios
     * @return String con el texto resultante
     */
    private String rellenarConEspacios(String texto, int tam)
    {
        for (int i = texto.length(); i < tam; i++)
        {
            texto += " ";
        }
        
        return texto;
    }
    
    /**
     * Escribe una nueva linea en la consola
     * 
     * @param texto String con el texto a escribir
     */
    private void escribirnl(String texto)
    {
        try
        {
            txtConsola.setText(txtConsola.getText() + texto + '\n');
            Thread.sleep(25);
            JScrollBar vertical = scrollPrincipal.getVerticalScrollBar();
            vertical.setValue(vertical.getMaximum());
        }
            catch (InterruptedException ex)
        {
            ex.printStackTrace();
        }
    }
    
    /**
     * Escribe en la consola
     * 
     * @param texto String con el texto a escribir
     */
    private void escribir(String texto)
    {
        txtConsola.setText(txtConsola.getText() + texto);
    }
    
    /**
     * Lee un comando introducido por el usuario
     * @return String texto leido
     */
    private String leer()
    {
        String lectura;
        
        //Mientras no se ha enviado ningún comando, esperamos
        try
        {
            while (!nuevoComando)
                Thread.sleep(100);
            
            lectura = txtComando.getText();
            txtComando.setText("");
            txtComando.requestFocus();
            
            nuevoComando = false;
            if (txtComando.getEchoChar() == '*')
                escribirnl(lectura.replaceAll(".", "*"));
            else
                escribirnl(lectura);
        }
        catch (InterruptedException ex)
        {
            ex.printStackTrace();
            lectura = null;
        }
        
        
        return lectura;
    }
    
    /**
     * Obtiene la cadena de conexión desde el fichero de configuración
     * 
     * @return String con la cadena de conexión
     */
    private String getCadenaConexion()
    {
        String cadenaConexion = "";
        
        if (IO.existe(rutaCfg))
        {
            String url = "";
            String bd = "";
            String user = "";
            String password = "";

            //Leemos el fichero
            String cfg = IO.leerFichero(rutaCfg);
            
            //Lo desciframos
            cfg = CifradoAES.descifrar(cfg);
            
            //Buscamos la información
            String lineas[] = cfg.split("\n");

            for (String linea : lineas)
            {
                if (linea.startsWith("Url"))
                {
                    url = "jdbc:" + linea.substring("Url".length()+1);
                }
                else if (linea.startsWith("Database"))
                {
                    bd = linea.substring("Database".length()+1);
                }
                else if (linea.startsWith("User"))
                {
                    user = linea.substring("User".length()+1);
                }
                else if (linea.startsWith("Password"))
                {
                    try
                    {
                        password = linea.substring("Password".length()+1);
                    }
                    //Sin contraseña
                    catch (IndexOutOfBoundsException ex)
                    {
                        password = "";
                    }
                }
            }

            //Añadimos la barra al final de la ruta si el usuario no la ha incluido
            if (!url.endsWith("/"))
                url += "/";
            
            cadenaConexion = url + bd + "@" + user+ "@" + password;
        }
        else
            escribirnl("No se encontró el fichero de configuración \"cipheredConfig.cfg\", colócalo\n"
                    + " en la misma ruta que el ejecutable o crea uno nuevo seleccionando la opción 2.");
        
        return cadenaConexion;
    }
    
    /**
     * Establece una nueva cadena de conexión en el fichero de configuración
     */
    private void nuevaCadenaConexion()
    {
        String cfg;
        String url;
        String bd;
        String user;
        String password;
        boolean campoValido;
        
        
        escribirnl("Estableciendo nuevos datos de conexión:\n");

        escribirnl("URL de conexión al SGBD (mysql://ip:puerto):");
        url = "Url " + leer();
        escribirnl("Nombre de la base de datos:");
        bd = "Database " + leer();
        escribirnl("Nombre de usuario de la base de datos:");
        user = "User " + leer();
        escribirnl("Contraseña del usuario de la base de datos:");
        txtComando.setEchoChar('*');
        password = "Password " + leer();
        txtComando.setEchoChar((char)0);
        
        //Creamos la información que se guardara en el fichero de configuración
        cfg = url + "\n" + bd + "\n" + user + "\n" + password;
        
        do
        {
            String guardar;
            
            escribir("¿Seguro que deseas establecer unos nuevos datos de conexión? El fichero de configuración se sobreescribirá (S/N): ");
            guardar = leer();
            
            if (guardar.toLowerCase().startsWith("s") || guardar.toLowerCase().startsWith("n"))
            {
                campoValido = true;
                
                if (guardar.toLowerCase().startsWith("s"))
                {
                    //Ciframos la información
                    cfg = CifradoAES.cifrar(cfg);

                    //Escribimos en el fichero los datos cifrados
                    IO.escribirFichero(rutaCfg, cfg);
                    escribirnl("Fichero de configuración modificado correctamente.");
                }
                else
                    escribirnl("Acción cancelada");
            }
            else
            {
                campoValido = false;
                escribirnl("Error: Por favor especifica si desea guardar o cancelar.");
            }
            
        }while (!campoValido);
    }
    
    /**
     * Muestra la asignaturas (nombre en el idioma base) del sistema y las devuelve.
     * 
     * @return ArrayList con la lista de asignaturas
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private ArrayList<ConfiguracionAsignaturaIdiomaEN> verAsignaturas() throws CADException
    {
        ArrayList<ConfiguracionAsignaturaIdiomaEN> confs;
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        
        confs = configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipalAsignaturas();
        
        escribirnl("\n" + rellenarConEspacios("ID", 10) + rellenarConEspacios("Asignatura", 50) + rellenarConEspacios("Idioma principal", 30) + "Nº de opciones");
        escribirnl("--------------------------------------------------------------------------------------------------------");
        
        for (int i = 0; i < confs.size(); i++)
        {
            escribir(rellenarConEspacios(String.valueOf(i+1), 10));
            escribir(rellenarConEspacios(confs.get(i).getNombreAsignatura(), 50));
            escribir(rellenarConEspacios(confs.get(i).getIdioma().getNombre(), 30));
            escribirnl(String.valueOf(confs.get(i).getAsignatura().getNumeroOpciones()));
        }
        
        escribir("\n");
        
        return confs;
    }
    
    /**
     * Muestra y devuelve la lista de usuarios del sistema
     * @return ArrayList con la lista de usuarios del sistema
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private ArrayList<UsuarioEN> verUsuarios() throws CADException
    {
        ArrayList<UsuarioEN> usuarios;
        UsuarioCEN usuarioCEN = new UsuarioCEN();
        
        usuarios = usuarioCEN.getUsuarios();
        
        escribirnl("\n" + rellenarConEspacios("ID", 10) + rellenarConEspacios("Usuario", 30) + "Administrador");
        escribirnl("-----------------------------------------------------");
        
        for (int i = 0; i < usuarios.size(); i++)
        {
            escribir(rellenarConEspacios(String.valueOf(i+1), 10));
            escribir(rellenarConEspacios(usuarios.get(i).getUser(), 30));
            if (usuarios.get(i).isAdministrador())
                escribirnl("Si");
            else
                escribirnl("No");
        }
        
        escribir("\n");
        
        return usuarios;
        
    }
    
    /**
     * Muestra y devuelve la lista de idiomas del sistema
     * @return ArrayList con la lista de idiomas del sistema
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private ArrayList<IdiomaEN> verIdiomas() throws CADException
    {
        ArrayList<IdiomaEN> idiomas;
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        
        idiomas = idiomaCEN.getIdiomas();
        
        escribirnl("\n" + rellenarConEspacios("ID", 10) + rellenarConEspacios("Idioma", 20) + rellenarConEspacios("Paquete babel", 20)
                + rellenarConEspacios("Trac. Duración", 20) + rellenarConEspacios("Trac. Instrucciones", 25) + rellenarConEspacios("Trac. Modalidad", 20)
                + rellenarConEspacios("Trac. Preguntas", 20) + "Respuestas");
        escribirnl("-------------------------------------------------------------------------------------------------------------------------------------------------");
        
        for (int i = 0; i < idiomas.size(); i++)
        {
            escribir(rellenarConEspacios(String.valueOf(i+1), 10));
            escribir(rellenarConEspacios(idiomas.get(i).getNombre(), 20));
            escribir(rellenarConEspacios(idiomas.get(i).getPaqueteBabel(), 20));
            escribir(rellenarConEspacios(idiomas.get(i).getTraduccionDuracion(), 20));
            escribir(rellenarConEspacios(idiomas.get(i).getTraduccionInstrucciones(), 25));
            escribir(rellenarConEspacios(idiomas.get(i).getTraduccionModalidad(), 20));
            escribir(rellenarConEspacios(idiomas.get(i).getTraduccionPreguntas(), 20));
            escribirnl(idiomas.get(i).getTraduccionRespuestas());
            
        }
        
        escribir("\n");
        
        return idiomas;
    }
    
    /**
     * Crea una nueva asignatura en el sistema
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private void crearAsignatura() throws CADException
    {
        int idIdioma = 0;
        int idUsuario = 0;
        boolean campoValido;
        escribirnl("Creando Asignatura:\n");
        
        ArrayList<IdiomaEN> idiomas = verIdiomas();        

        if (idiomas.size() > 0)
        {
            AsignaturaCEN asignaturaCEN = new AsignaturaCEN();
            AccesoUsuarioAsignaturaCEN accesoUsuarioAsignaturaCEN = new AccesoUsuarioAsignaturaCEN();
            ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
            ConfiguracionAsignaturaIdiomaEN conf = new ConfiguracionAsignaturaIdiomaEN();
            AsignaturaEN asignaturaEN = new AsignaturaEN();
            AccesoUsuarioAsignaturaEN accesoUsuarioAsignaturaEN = new AccesoUsuarioAsignaturaEN();
            
            do {
                try
                {
                    escribirnl("Selecciona el idioma principal que se utilizará en la asignatura: ");
                    String idStr = leer();
                    idIdioma = Integer.parseInt(idStr);

                    if (idIdioma > 0 && idIdioma <= idiomas.size())
                    {
                        //El array empieza por 0
                        idIdioma--;
                        campoValido = true;
                    }
                    else
                        throw new NumberFormatException();
                }
                catch(NumberFormatException ex)
                {
                    escribirnl("Opción Inválida\n");
                    campoValido = false;
                }
            }
            while(!campoValido);

            do
            {
                escribir("Nombre de la asignatura (en \"" + idiomas.get(idIdioma).getNombre() + "\"): ");
                conf.setNombreAsignatura(leer());

                if (conf.getNombreAsignatura().length() == 0)
                {
                    campoValido = false;
                    escribirnl("Error: El nombre de la asignatura no puede estar en blanco");
                }
                else
                    campoValido = true;

            }while (!campoValido);

            do
            {
                escribir("Número de opciones que tendrán las preguntas en los exámenes: ");
                
                try
                {
                    asignaturaEN.setNumeroOpciones(Integer.parseInt(leer()));
                    campoValido = true;
                }
                catch(NumberFormatException ex)
                {
                    campoValido = false;
                    escribirnl("Error: Introduce un número válido.");
                }
            }
            while(!campoValido);
            
            ArrayList<UsuarioEN> usuarios = verUsuarios();
            
            do {
                try
                {
                    escribirnl("Selecciona el usuario que administrará la asignatura: ");
                    String idStr = leer();
                    idUsuario = Integer.parseInt(idStr);

                    if (idUsuario > 0 && idUsuario <= usuarios.size())
                    {
                        //El array empieza por 0
                        idUsuario--;
                        campoValido = true;
                    }
                    else
                        throw new NumberFormatException();
                }
                catch(NumberFormatException ex)
                {
                    escribirnl("Opción Inválida\n");
                    campoValido = false;
                }
            }
            while(!campoValido);
            
            do
            {
                String guardar;

                escribir("¿Desea guardar el elemento creado? (S/N): ");
                guardar = leer();

                if (guardar.toLowerCase().startsWith("s") || guardar.toLowerCase().startsWith("n"))
                {
                    campoValido = true;

                    if (guardar.toLowerCase().startsWith("s"))
                    {
                        asignaturaCEN.New(asignaturaEN);
                        
                        conf.setAsignatura(asignaturaEN);
                        conf.setIdioma(idiomas.get(idIdioma));
                        conf.setIdiomaPrincipal(true);
                        configuracionAsignaturaIdiomaCEN.New(conf);
                        
                        accesoUsuarioAsignaturaEN.setAdministrador(true);
                        accesoUsuarioAsignaturaEN.setAsignatura(asignaturaEN);
                        accesoUsuarioAsignaturaEN.setUsuario(usuarios.get(idUsuario));
                        
                        accesoUsuarioAsignaturaCEN.New(accesoUsuarioAsignaturaEN);
                        
                        escribirnl("Asignatura creada correctamente.");
                    }
                }
                else
                {
                    campoValido = false;
                    escribirnl("Error: Por favor especifica si desea guardar o cancelar.");
                }

            }while (!campoValido);
        }
        else
            escribirnl("No existen idiomas disponibles para crear una asignatura.");
    }
    
    /**
     * Crea un nuevo usuario en el sistema
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private void crearUsuario() throws CADException
    {
        UsuarioEN usuarioEN = new UsuarioEN();
        UsuarioCEN usuarioCEN = new UsuarioCEN();
        boolean campoValido;
        
        escribirnl("Creando Usuario:\n");
        
        do
        {
            escribir("Nombre usuario: ");
            usuarioEN.setUser(leer());
            
            if (usuarioCEN.getUsuarioByNombre(usuarioEN.getUser()) != null)
            {
                campoValido = false;
                escribirnl("Error: Ya existe un usuario con ese nombre.");
            }
            else
                campoValido = true;
            
        }while (!campoValido);
        
        do
        {
            escribir("Contraseña: ");
            txtComando.setEchoChar('*');
            usuarioEN.setPassword(leer());
            txtComando.setEchoChar((char)0);
            
            if (usuarioEN.getPassword().length() == 0 || usuarioEN.getPassword().contains(" "))
            {
                campoValido = false;
                escribirnl("Error: La contraseña no puede estar en blanco ni contener espacios.");
            }
            else
                campoValido = true;
            
        }while (!campoValido);
        
        do
        {
            String adminStr;
            
            escribir("Administrador del sistema (S/N): ");
            adminStr = leer();
            
            if (adminStr.toLowerCase().startsWith("s") || adminStr.toLowerCase().startsWith("n"))
            {
                campoValido = true;
                usuarioEN.setAdministrador(adminStr.toLowerCase().startsWith("s"));
            }
            else
            {
                campoValido = false;
                escribirnl("Error: Por favor especifica si el usuario será administrador del sistema.");
            }
            
        }while (!campoValido);
        
        do
        {
            String guardar;
            
            escribir("¿Desea guardar el elemento creado? (S/N): ");
            guardar = leer();
            
            if (guardar.toLowerCase().startsWith("s") || guardar.toLowerCase().startsWith("n"))
            {
                campoValido = true;
                
                if (guardar.toLowerCase().startsWith("s"))
                {
                    usuarioCEN.New(usuarioEN);
                    escribirnl("Usuario creado correctamente.");
                }
            }
            else
            {
                campoValido = false;
                escribirnl("Error: Por favor especifica si desea guardar o cancelar.");
            }
            
        }while (!campoValido);
    }
    
    /**
     * Crea un nuevo idioma en el sistema
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private void crearIdioma() throws CADException
    {
        IdiomaEN idiomaEN = new IdiomaEN();
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        

        boolean campoValido;
        
        escribirnl("Creando Idioma:\n");
        
        do
        {
            escribir("Nombre: ");
            idiomaEN.setNombre(leer());
            escribirnl(idiomaEN.getNombre());
            if (idiomaCEN.getIdiomaByNombre(idiomaEN.getNombre()) != null)
            {
                campoValido = false;
                escribirnl("Error: Ya existe un idioma con ese nombre.");
            }
            else if (idiomaEN.getNombre().length() == 0)
            {
                campoValido = false;
                escribirnl("Error: El nombre del idioma no puede estar en blanco.");
            }
            else
                campoValido = true;
            
        }while (!campoValido);
        
        do
        {
            escribir("Paquete babel para generar LaTeX en este idioma (opcional): ");
            idiomaEN.setPaqueteBabel(leer());
            
            if (idiomaEN.getPaqueteBabel().length() == 0)
                idiomaEN.setPaqueteBabel(null);
            
            campoValido = true;
            
        }while (!campoValido);
        
        do
        {
            escribir("Traducción de la siguiente palabra en este idioma (Duración): ");
            idiomaEN.setTraduccionDuracion(leer());
            
            if (idiomaEN.getTraduccionDuracion().length() == 0)
            {
                campoValido = false;
                escribirnl("Error: La traducción de la palabra no puede dejarse en blanco.");
            }
            else
                campoValido = true;
            
        }while (!campoValido);
        
        do
        {
            escribir("Traducción de la siguiente palabra en este idioma (Instrucciones): ");
            idiomaEN.setTraduccionInstrucciones(leer());
            
            if (idiomaEN.getTraduccionInstrucciones().length() == 0)
            {
                campoValido = false;
                escribirnl("Error: La traducción de la palabra no puede dejarse en blanco.");
            }
            else
                campoValido = true;
            
        }while (!campoValido);
        
        do
        {
            escribir("Traducción de la siguiente palabra en este idioma (Modalidad): ");
            idiomaEN.setTraduccionModalidad(leer());
            
            if (idiomaEN.getTraduccionModalidad().length() == 0)
            {
                campoValido = false;
                escribirnl("Error: La traducción de la palabra no puede dejarse en blanco.");
            }
            else
                campoValido = true;
            
        }while (!campoValido);
        
        do
        {
            escribir("Traducción de la siguiente palabra en este idioma (Preguntas): ");
            idiomaEN.setTraduccionPreguntas(leer());
            
            if (idiomaEN.getTraduccionPreguntas().length() == 0)
            {
                campoValido = false;
                escribirnl("Error: La traducción de la palabra no puede dejarse en blanco.");
            }
            else
                campoValido = true;
            
        }while (!campoValido);
        
        do
        {
            escribir("Traducción de la siguiente palabra en este idioma (Respuestas): ");
            idiomaEN.setTraduccionRespuestas(leer());
            
            if (idiomaEN.getTraduccionRespuestas().length() == 0)
            {
                campoValido = false;
                escribirnl("Error: La traducción de la palabra no puede dejarse en blanco.");
            }
            else
                campoValido = true;
            
        }while (!campoValido);
        
        do
        {
            String guardar;
            
            escribir("¿Desea guardar el elemento creado? (S/N): ");
            guardar = leer();
            
            if (guardar.toLowerCase().startsWith("s") || guardar.toLowerCase().startsWith("n"))
            {
                campoValido = true;
                
                if (guardar.toLowerCase().startsWith("s"))
                {
                    idiomaCEN.New(idiomaEN);
                    escribirnl("Idioma creado correctamente.");
                }
            }
            else
            {
                campoValido = false;
                escribirnl("Error: Por favor especifica si desea guardar o cancelar.");
            }
            
        }while (!campoValido);
        
    }
    
    /**
     * Edita una asignatura del sistema
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private void editarAsignatura() throws CADException
    {
        int id = 0;
        boolean campoValido;
        AsignaturaCEN asignaturaCEN = new AsignaturaCEN();
        ConfiguracionAsignaturaIdiomaEN conf;
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        
        ArrayList<ConfiguracionAsignaturaIdiomaEN> asignaturas = verAsignaturas();
        
        if (asignaturas.size() > 0)
        {
            do
            {
                try
                {
                    escribirnl("Selecciona la asignatura de la lista que deseas editar: ");
                    String idStr = leer();
                    id = Integer.parseInt(idStr);

                    if (id > 0 && id <= asignaturas.size())
                    {
                        //El array empieza por 0
                        id--;
                        campoValido = true;
                    }
                    else
                        throw new NumberFormatException();
                }
                catch(NumberFormatException ex)
                {
                    escribirnl("Opción Inválida\n");
                    campoValido = false;
                }
            }
            while(!campoValido);

            conf = asignaturas.get(id);
            
            do
            {
                escribir("Nombre de la asignatura (en \"" + conf.getIdioma().getNombre() + "\"): ");
                conf.setNombreAsignatura(leer());

                if (conf.getNombreAsignatura().length() == 0)
                {
                    campoValido = false;
                    escribirnl("Error: El nombre de la asignatura no puede estar en blanco");
                }
                else
                    campoValido = true;

            }while (!campoValido);

            do
            {
                String guardar;

                escribir("¿Desea guardar el elemento editado? (S/N): ");
                guardar = leer();

                if (guardar.toLowerCase().startsWith("s") || guardar.toLowerCase().startsWith("n"))
                {
                    campoValido = true;

                    if (guardar.toLowerCase().startsWith("s"))
                    {
                        asignaturaCEN.Modify(conf.getAsignatura());
                        configuracionAsignaturaIdiomaCEN.Modify(conf);
                        
                        escribirnl("Asignatura editada correctamente.");
                    }
                }
                else
                {
                    campoValido = false;
                    escribirnl("Error: Por favor especifica si desea guardar o cancelar.");
                }

            }while (!campoValido);
        }
    }
    
    /**
     * Edita un usuario del sistema
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private void editarUsuario() throws CADException
    {
        UsuarioEN usuarioEN;
        int id = 0;
        boolean campoValido;
        UsuarioCEN usuarioCEN = new UsuarioCEN();
        ArrayList<UsuarioEN> usuarios = verUsuarios();
        
        if (usuarios.size() > 0)
        {
            do
            {
                try
                {
                    escribirnl("Selecciona el usuario de la lista que deseas editar: ");
                    String idStr = leer();
                    id = Integer.parseInt(idStr);

                    if (id > 0 && id <= usuarios.size())
                    {
                        //El array empieza por 0
                        id--;
                        campoValido = true;
                    }
                    else
                        throw new NumberFormatException();
                }
                catch(NumberFormatException ex)
                {
                    escribirnl("Opción Inválida\n");
                    campoValido = false;
                }
            }
            while(!campoValido);
            
            usuarioEN = usuarios.get(id);
        
            do
            {
                escribir("Nombre usuario: ");
                txtComando.setText(usuarioEN.getUser());
                String nombre = leer();

                if (!usuarioEN.getUser().equals(nombre) && usuarioCEN.getUsuarioByNombre(nombre) != null)
                {
                    campoValido = false;
                    escribirnl("Error: Ya existe un usuario con ese nombre.");
                }
                else
                {
                    usuarioEN.setUser(nombre);
                    campoValido = true;
                }

            }while (!campoValido);

            do
            {
                escribir("Contraseña: ");
                txtComando.setText(usuarioEN.getPassword());
                txtComando.setEchoChar('*');
                usuarioEN.setPassword(leer());
                txtComando.setEchoChar((char)0);

                if (usuarioEN.getPassword().length() == 0 || usuarioEN.getPassword().contains(" "))
                {
                    campoValido = false;
                    escribirnl("Error: La contraseña no puede estar en blanco ni contener espacios.");
                }
                else
                    campoValido = true;

            }while (!campoValido);

            //no se permite modificar el status de administrador al propio usuario
            if (usuarioEN.getCodigo() != usuarioLogin.getCodigo())
            {
                do
                {
                    String adminStr;

                    escribir("Administrador del sistema (S/N): ");
                    if (usuarioEN.isAdministrador())
                        txtComando.setText("S");
                    else
                        txtComando.setText("N");
                    adminStr = leer();

                    if (adminStr.toLowerCase().startsWith("s") || adminStr.toLowerCase().startsWith("n"))
                    {
                        campoValido = true;
                        usuarioEN.setAdministrador(adminStr.toLowerCase().startsWith("s"));
                    }
                    else
                    {
                        campoValido = false;
                        escribirnl("Error: Por favor especifica si el usuario será administrador del sistema.");
                    }

                }while (!campoValido);
            }

            do
            {
                String guardar;

                escribir("¿Desea guardar el elemento editado? (S/N): ");
                guardar = leer();

                if (guardar.toLowerCase().startsWith("s") || guardar.toLowerCase().startsWith("n"))
                {
                    campoValido = true;

                    if (guardar.toLowerCase().startsWith("s"))
                    {
                        usuarioCEN.Modify(usuarioEN);
                        escribirnl("Usuario editado correctamente.");
                    }
                }
                else
                {
                    campoValido = false;
                    escribirnl("Error: Por favor especifica si desea guardar o cancelar.");
                }

            }while (!campoValido);
        }
    }
    
    /**
     * Edita un idioma del sistema
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private void editarIdioma() throws CADException
    {
        IdiomaEN idiomaEN;
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        boolean campoValido;
        
        int id = 0;
        ArrayList<IdiomaEN> idiomas = verIdiomas();
        
        if (idiomas.size() > 0)
        {
            do
            {
                try
                {
                    escribirnl("Selecciona el idioma de la lista que deseas editar: ");
                    String idStr = leer();
                    id = Integer.parseInt(idStr);

                    if (id > 0 && id <= idiomas.size())
                    {
                        //El array empieza por 0
                        id--;
                        campoValido = true;
                    }
                    else
                        throw new NumberFormatException();
                }
                catch(NumberFormatException ex)
                {
                    escribirnl("Opción Inválida\n");
                    campoValido = false;
                }
            }while(!campoValido);
        
            idiomaEN = idiomas.get(id);
            
            do
            {
                escribir("Nombre: ");
                txtComando.setText(idiomaEN.getNombre());
                String nombre = leer();
                escribirnl(nombre);
                if (!idiomaEN.getNombre().equals(nombre) && idiomaCEN.getIdiomaByNombre(nombre) != null)
                {
                    campoValido = false;
                    escribirnl("Error: Ya existe un idioma con ese nombre.");
                }
                else if (idiomaEN.getNombre().length() == 0)
                {
                    campoValido = false;
                    escribirnl("Error: El nombre del idioma no puede estar en blanco.");
                }
                else
                {
                    idiomaEN.setNombre(nombre);
                    campoValido = true;
                }

            }while (!campoValido);

            do
            {
                escribir("Paquete babel para generar LaTeX en este idioma (opcional): ");
                txtComando.setText(idiomaEN.getPaqueteBabel());
                idiomaEN.setPaqueteBabel(leer());

                if (idiomaEN.getPaqueteBabel().length() == 0)
                    idiomaEN.setPaqueteBabel(null);

                campoValido = true;

            }while (!campoValido);

            do
            {
                escribir("Traducción de la siguiente palabra en este idioma (Duración): ");
                txtComando.setText(idiomaEN.getTraduccionDuracion());
                idiomaEN.setTraduccionDuracion(leer());

                if (idiomaEN.getTraduccionDuracion().length() == 0)
                {
                    campoValido = false;
                    escribirnl("Error: La traducción de la palabra no puede dejarse en blanco.");
                }
                else
                    campoValido = true;

            }while (!campoValido);

            do
            {
                escribir("Traducción de la siguiente palabra en este idioma (Instrucciones): ");
                txtComando.setText(idiomaEN.getTraduccionInstrucciones());
                idiomaEN.setTraduccionInstrucciones(leer());

                if (idiomaEN.getTraduccionInstrucciones().length() == 0)
                {
                    campoValido = false;
                    escribirnl("Error: La traducción de la palabra no puede dejarse en blanco.");
                }
                else
                    campoValido = true;

            }while (!campoValido);

            do
            {
                escribir("Traducción de la siguiente palabra en este idioma (Modalidad): ");
                txtComando.setText(idiomaEN.getTraduccionModalidad());
                idiomaEN.setTraduccionModalidad(leer());

                if (idiomaEN.getTraduccionModalidad().length() == 0)
                {
                    campoValido = false;
                    escribirnl("Error: La traducción de la palabra no puede dejarse en blanco.");
                }
                else
                    campoValido = true;

            }while (!campoValido);

            do
            {
                escribir("Traducción de la siguiente palabra en este idioma (Preguntas): ");
                txtComando.setText(idiomaEN.getTraduccionPreguntas());
                idiomaEN.setTraduccionPreguntas(leer());

                if (idiomaEN.getTraduccionPreguntas().length() == 0)
                {
                    campoValido = false;
                    escribirnl("Error: La traducción de la palabra no puede dejarse en blanco.");
                }
                else
                    campoValido = true;

            }while (!campoValido);

            do
            {
                escribir("Traducción de la siguiente palabra en este idioma (Respuestas): ");
                txtComando.setText(idiomaEN.getTraduccionRespuestas());
                idiomaEN.setTraduccionRespuestas(leer());

                if (idiomaEN.getTraduccionRespuestas().length() == 0)
                {
                    campoValido = false;
                    escribirnl("Error: La traducción de la palabra no puede dejarse en blanco.");
                }
                else
                    campoValido = true;

            }while (!campoValido);

            do
            {
                String guardar;

                escribir("¿Desea guardar el elemento editado? (S/N): ");
                guardar = leer();

                if (guardar.toLowerCase().startsWith("s") || guardar.toLowerCase().startsWith("n"))
                {
                    campoValido = true;

                    if (guardar.toLowerCase().startsWith("s"))
                    {
                        idiomaCEN.Modify(idiomaEN);
                        escribirnl("Idioma editado correctamente.");
                    }
                }
                else
                {
                    campoValido = false;
                    escribirnl("Error: Por favor especifica si desea guardar o cancelar.");
                }

            }while (!campoValido);
        }
        
    }
    
    /**
     * Elimina una asignatura del sistema
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private void eliminarAsignatura() throws CADException
    {
        int id = 0;
        boolean campoValido;
        AsignaturaCEN asignaturaCEN = new AsignaturaCEN();
        ArrayList<ConfiguracionAsignaturaIdiomaEN> asignaturas = verAsignaturas();
        
        if (asignaturas.size() > 0)
        {
            do
            {
                try
                {
                    escribirnl("Selecciona la asignatura de la lista que deseas eliminar: ");
                    String idStr = leer();
                    id = Integer.parseInt(idStr);

                    if (id > 0 && id <= asignaturas.size())
                    {
                        //El array empieza por 0
                        id--;
                        campoValido = true;
                    }
                    else
                        throw new NumberFormatException();
                }
                catch(NumberFormatException ex)
                {
                    escribirnl("Opción Inválida\n");
                    campoValido = false;
                }
            }
            while(!campoValido);
            
            do
            {
                String guardar;

                escribir("¿Seguro que desea borrar la asignatura seleccionada? Se perderá toda la información almacenada en esta asignatura (S/N): ");
                guardar = leer();

                if (guardar.toLowerCase().startsWith("s") || guardar.toLowerCase().startsWith("n"))
                {
                    campoValido = true;

                    if (guardar.toLowerCase().startsWith("s"))
                    {
                        asignaturaCEN.Delete(asignaturas.get(id).getAsignatura());
                        escribirnl("La asignatura " + asignaturas.get(id).getNombreAsignatura() + " ha sido eliminada");
                    }
                    else
                    {
                        escribirnl("Acción cancelada.");
                    }
                }
                else
                {
                    campoValido = false;
                    escribirnl("Error: Por favor confirme la operacion.");
                }

            }while (!campoValido);
        }
        else
            escribirnl("No existe ninguna asignatura en el sistema.");
        
    }
    
    /**
     * Elimina un usuario del sistema
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private void eliminarUsuario() throws CADException
    {
        int id = 0;
        boolean campoValido;
        UsuarioCEN usuarioCEN = new UsuarioCEN();
        ArrayList<UsuarioEN> usuarios = verUsuarios();
        AccesoUsuarioAsignaturaCEN accesoUsuarioAsignaturaCEN = new AccesoUsuarioAsignaturaCEN();
        ArrayList<AccesoUsuarioAsignaturaEN> accesosUsuarioUnicoAdmin;
        
        if (usuarios.size() > 0)
        {
            do
            {
                try
                {
                    escribirnl("Selecciona el usuario de la lista que deseas eliminar: ");
                    String idStr = leer();
                    id = Integer.parseInt(idStr);

                    if (id > 0 && id <= usuarios.size())
                    {
                        //El array empieza por 0
                        id--;
                        campoValido = true;
                    }
                    else
                        throw new NumberFormatException();
                }
                catch(NumberFormatException ex)
                {
                    escribirnl("Opción Inválida\n");
                    campoValido = false;
                }
            }
            while(!campoValido);
            
            accesosUsuarioUnicoAdmin = accesoUsuarioAsignaturaCEN.getAccesosUsuarioUnicoAdmin(usuarios.get(id));
            
            if (accesosUsuarioUnicoAdmin.size() <= 0)
            {
                if (id != usuarioLogin.getCodigo())
                {
                    do
                    {
                        String guardar;

                        escribir("¿Seguro que desea borrar el usuario seleccionado? Esta acción es irreversible (S/N): ");
                        guardar = leer();

                        if (guardar.toLowerCase().startsWith("s") || guardar.toLowerCase().startsWith("n"))
                        {
                            campoValido = true;

                            if (guardar.toLowerCase().startsWith("s"))
                            {
                                usuarioCEN.Delete(usuarios.get(id));
                                escribirnl("El usuario " + usuarios.get(id).getUser() + " ha sido eliminado");
                            }
                            else
                            {
                                escribirnl("Acción cancelada.");
                            }
                        }
                        else
                        {
                            campoValido = false;
                            escribirnl("Error: Por favor confirme la operacion.");
                        }

                    }while (!campoValido);
                }
                else
                {
                     escribirnl("Error: Por motivos de seguridad no se permite que borres tu propio usuario.");
                }
            }
            else
            {
                ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
                
                escribirnl("Error: El usuario no puede ser eliminado porque las siguientes asignaturas dependen de él (es el único administrador): ");
                for (AccesoUsuarioAsignaturaEN accesoUsuarioUnicoAdmin : accesosUsuarioUnicoAdmin)
                {
                    ConfiguracionAsignaturaIdiomaEN conf = configuracionAsignaturaIdiomaCEN.getConfiguracionIdiomaPrincipal(accesoUsuarioUnicoAdmin.getAsignatura());
                    escribirnl("- " + conf.getNombreAsignatura());
                }
            }
        }
        else
            escribirnl("No existe ningún usuario en el sistema (¿es esto posible?)");
        
    }
    
    /**
     * Elimina un idioma del sistema
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private void eliminarIdioma() throws CADException
    {
        int id = 0;
        boolean campoValido;
        IdiomaCEN idiomaCEN = new IdiomaCEN();
        ArrayList<IdiomaEN> idiomas = verIdiomas();
        ConfiguracionAsignaturaIdiomaCEN configuracionAsignaturaIdiomaCEN = new ConfiguracionAsignaturaIdiomaCEN();
        int asignaturasDependientes;
        
        if (idiomas.size() > 0)
        {
            do
            {
                try
                {
                    escribirnl("Selecciona el idioma de la lista que deseas eliminar: ");
                    String idStr = leer();
                    id = Integer.parseInt(idStr);

                    if (id > 0 && id <= idiomas.size())
                    {
                        //El array empieza por 0
                        id--;
                        campoValido = true;
                    }
                    else
                        throw new NumberFormatException();
                }
                catch(NumberFormatException ex)
                {
                    escribirnl("Opción Inválida\n");
                    campoValido = false;
                }
            }
            while(!campoValido);
            
            asignaturasDependientes = configuracionAsignaturaIdiomaCEN.getConfiguracionesPrincipalesByIdioma(idiomas.get(id)).size();
            
            if (asignaturasDependientes == 0)
            {
                do
                {
                    String guardar;

                    escribir("¿Seguro que desea borrar el idioma seleccionado? Se perdera toda la información almacenada en este idioma (S/N): ");
                    guardar = leer();

                    if (guardar.toLowerCase().startsWith("s") || guardar.toLowerCase().startsWith("n"))
                    {
                        campoValido = true;

                        if (guardar.toLowerCase().startsWith("s"))
                        {
                            idiomaCEN.Delete(idiomas.get(id));
                            escribirnl("El idioma " + idiomas.get(id).getNombre() + " ha sido eliminado");
                        }
                        else
                        {
                            escribirnl("Acción cancelada.");
                        }
                    }
                    else
                    {
                        campoValido = false;
                        escribirnl("Error: Por favor confirme la operacion.");
                    }

                }while (!campoValido);
            }
            else
                escribirnl("No se ha podido eliminar.\nExiste/n " + asignaturasDependientes + " asignatura/s que depende/n de este idioma para funcionar.");
        }
        else
            escribirnl("No existe ninguna asignatura en el sistema.");
        
    }
    
    /**
     * Gestiona el menu de las asignaturas
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private void gestionAsignaturas() throws CADException
    {
        int opcion = menuAsignaturas();
            
        //Mostramos el menú principal hasta que se decida salir
        while (opcion != 5)
        {
            switch(opcion)
            {
                //Nueva
                case 1:
                    crearAsignatura();
                    break;
                //Editar
                case 2:
                    editarAsignatura();
                    break;
                //Eliminar
                case 3:
                    eliminarAsignatura();
                    break;
                //Ver
                case 4:
                    verAsignaturas();
                    break;
            }

            opcion = menuAsignaturas(); 
        }
    }
    
    /**
     * Gestiona el menu de los usuarios
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private void gestionUsuarios() throws CADException
    {
        int opcion = menuUsuarios();
            
        //Mostramos el menú principal hasta que se decida salir
        while (opcion != 5)
        {
            switch(opcion)
            {
                //Nueva
                case 1:
                    crearUsuario();
                    break;
                //Editar
                case 2:
                    editarUsuario();
                    break;
                //Eliminar
                case 3:
                    eliminarUsuario();
                    break;
                //Ver
                case 4:
                    escribirnl("Listado de usuarios:\n");
                    verUsuarios();
                    break;
            }

            opcion = menuUsuarios(); 
        }
    }
    
    /**
     * Gestiona el menu de los idiomas
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private void gestionIdiomas() throws CADException
    {
        int opcion = menuIdiomas();
            
        //Mostramos el menú principal hasta que se decida salir
        while (opcion != 5)
        {
            switch(opcion)
            {
                //Nueva
                case 1:
                    crearIdioma();
                    break;
                //Editar
                case 2:
                    editarIdioma();
                    break;
                //Eliminar
                case 3:
                    eliminarIdioma();
                    break;
                //Ver
                case 4:
                    escribirnl("Listado de idiomas:\n");
                    verIdiomas();
                    break;
            }

            opcion = menuIdiomas(); 
        }
    }
 
    /**
     * Muestra el menú de las asignaturas
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private int menuAsignaturas()
    {
        String opcionStr;
        int opcion = 0;
        boolean valida = false;
        
        while (!valida)
        {
            escribirnl("\nGestión de Asignaturas:");
            escribirnl("---------------------------------");
            escribirnl("1.- Nueva Asignatura");
            escribirnl("2.- Editar Asignatura");
            escribirnl("3.- Eliminar Asignatura");
            escribirnl("4.- Ver Asignaturas");
            escribirnl("5.- Volver");
            escribirnl("---------------------------------");
            escribir("Opción: ");

            opcionStr = leer();

            try
            {
                opcion = Integer.parseInt(opcionStr);
                
                if (opcion > 0 && opcion < 6)
                    valida = true;
                else
                    throw new NumberFormatException();
            }
            catch(NumberFormatException ex)
            {
                escribirnl("Opción Inválida\n");
            }
        }
        
        return opcion;
    }
    
    /**
     * Muestra el menú de los usuarios
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private int menuUsuarios()
    {
        String opcionStr;
        int opcion = 0;
        boolean valida = false;
        
        while (!valida)
        {
            escribirnl("\nGestión de Usuarios:");
            escribirnl("---------------------------------");
            escribirnl("1.- Nuevo Usuario");
            escribirnl("2.- Editar Usuario");
            escribirnl("3.- Eliminar Usuario");
            escribirnl("4.- Ver Usuarios");
            escribirnl("5.- Volver");
            escribirnl("---------------------------------");
            escribir("Opción: ");

            opcionStr = leer();

            try
            {
                opcion = Integer.parseInt(opcionStr);
                
                if (opcion > 0 && opcion < 6)
                    valida = true;
                else
                    throw new NumberFormatException();
            }
            catch(NumberFormatException ex)
            {
                escribirnl("Opción Inválida\n");
            }
        }
        
        return opcion;
    }
    
    /**
     * Muestra el menú de los idiomas
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private int menuIdiomas()
    {
        String opcionStr;
        int opcion = 0;
        boolean valida = false;
        
        while (!valida)
        {
            escribirnl("\nGestión de Idiomas:");
            escribirnl("---------------------------------");
            escribirnl("1.- Nuevo Idioma");
            escribirnl("2.- Editar Idioma");
            escribirnl("3.- Eliminar Idioma");
            escribirnl("4.- Ver Idiomas");
            escribirnl("5.- Volver");
            escribirnl("---------------------------------");
            escribir("Opción: ");

            opcionStr = leer();

            try
            {
                opcion = Integer.parseInt(opcionStr);
                
                if (opcion > 0 && opcion < 6)
                    valida = true;
                else
                    throw new NumberFormatException();
            }
            catch(NumberFormatException ex)
            {
                escribirnl("Opción Inválida\n");
            }
        }
        
        return opcion;
    }
    
    /**
     * Muestra y gestiona el menú principal
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private int menuPrincipal()
    {
        String opcionStr;
        int opcion = 0;
        boolean valida = false;
        
        while (!valida)
        {
            escribirnl("\nMenú:");
            escribirnl("---------------------------------");
            escribirnl("1.- Gestión de Asignaturas");
            escribirnl("2.- Gestión de Usuarios");
            escribirnl("3.- Gestión de Idiomas");
            escribirnl("4.- Salir");
            escribirnl("---------------------------------");
            escribir("Opción: ");

            opcionStr = leer();

            try
            {
                opcion = Integer.parseInt(opcionStr);
                
                if (opcion > 0 && opcion < 5)
                    valida = true;
                else
                    throw new NumberFormatException();
            }
            catch(NumberFormatException ex)
            {
                escribirnl("Opción Inválida\n");
            }
        }
        
        return opcion;
    }
    
    /**
     * Muestra y gestiona el menú de identificación de usuarios
     * 
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private int menuIniciarSesion()
    {
        String opcionStr;
        int opcion = 0;
        boolean valida = false;
        
        while (!valida)
        {
            escribirnl("\nMenú:");
            escribirnl("---------------------------------");
            escribirnl("1.- Iniciar Sesión");
            escribirnl("2.- Modificar datos de conexión");
            escribirnl("3.- Salir");
            escribirnl("---------------------------------");
            escribir("Opción: ");

            opcionStr = leer();

            try
            {
                opcion = Integer.parseInt(opcionStr);
                
                if (opcion > 0 && opcion < 4)
                    valida = true;
                else
                    throw new NumberFormatException();
            }
            catch(NumberFormatException ex)
            {
                escribirnl("Opción Inválida\n");
            }
        }
        
        return opcion;
    }
    
    /**
     * Realiza el proceso de inicio de sesión de un usuario
     * @return UsuarioEN que ha iniciado sesión o null si no se ha conseguido 
     * iniciar sesión correctamente
     * @throws CADException Si ocurrió un error al acceder a la base de datos
     */
    private UsuarioEN iniciarSesion() throws CADException
    {
        String usuario, password;
        String conexion;
        UsuarioEN usuarioEN = null;
        UsuarioCEN usuarioCEN = new UsuarioCEN();
        
        conexion = getCadenaConexion();
        
        if (!conexion.equals(""))
        {
            AccesoBD.setCadenaConexion(conexion);

            //Pedimos los datos
            escribir("Nombre de usuario: ");
            usuario = leer();

            escribir("Contraseña: ");
            txtComando.setEchoChar('*');
            password = leer();
            txtComando.setEchoChar((char)0);

            //Obtenemos el usuario
            usuarioEN = usuarioCEN.getUsuarioByLogin(usuario, password);

            //Comprobamos el usuario
            if (usuarioEN == null)
                escribirnl("\nError en el inicio de sesión. Compruebe su usuario y contraseña.");
            else if (!usuarioEN.isAdministrador())
            {
                escribirnl("\nEste usuario no cuenta con los privilegios necesarios para la administración de la herramienta.");
                usuarioEN = null;
            }
            else
                escribirnl("\nSe ha iniciado sesión correctamente con el usuario: " + usuarioEN.getUser());
        }
        
        return usuarioEN;
    }

    /**
     * @param txtComando the txtComando to set
     */
    public void setTxtComando(JPasswordField txtComando) {
        this.txtComando = txtComando;
    }

    /**
     * @param txtConsola the txtConsola to set
     */
    public void setTxtConsola(JTextArea txtConsola) {
        this.txtConsola = txtConsola;
    }

    /**
     * @param nuevoComando the nuevoComando to set
     */
    public void setNuevoComando(boolean nuevoComando) {
        this.nuevoComando = nuevoComando;
    }

    /**
     * @param scrollPrincipal the scrollPrincipal to set
     */
    public void setScrollPrincipal(JScrollPane scrollPrincipal) {
        this.scrollPrincipal = scrollPrincipal;
    }
    
}
